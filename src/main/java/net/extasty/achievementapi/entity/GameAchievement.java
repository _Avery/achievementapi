package net.extasty.achievementapi.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Copyright ©_Avery 2016-2019
 * Erstellt von _Avery | Lukas am 2019-07-29 um 10:30
 **/

@Getter
@RequiredArgsConstructor
public class GameAchievement {

    private final String game;
    private final String name;
    private final String description;

}
