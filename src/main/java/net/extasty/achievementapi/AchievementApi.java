package net.extasty.achievementapi;

import net.extasty.achievementapi.entity.GameAchievement;

import java.util.List;
import java.util.UUID;

/**
 * Copyright ©_Avery 2016-2019
 * Erstellt von _Avery | Lukas am 2019-07-29 um 10:29
 **/
public interface AchievementApi {

    /**
     * Give an achievement to a specific user
     *
     * @param uuid            means the uuid of the player
     * @param gameAchievement means the achievement to add
     */
    void addAchievement(UUID uuid, GameAchievement gameAchievement);

    /**
     * Check if a player has a specific achievement
     *
     * @param uuid means the uuid of the player
     * @param game means the gamemode on the server
     * @param name means the name of the achievement
     * @return a boolean (if player has the achievement)
     */
    boolean hasAchievement(UUID uuid, String game, String name);

    /**
     * Get all achievement from a player with a given gamemode
     *
     * @param uuid means the player uuid
     * @param game means the gamemode on the server
     * @return list of all achievements from a player
     */
    List<String> getAchievements(UUID uuid, String game);

}
